import subprocess,os,time,sys,logging
from statsd import StatsClient
from systemd import journal
import logging.handlers
from systemd.daemon import notify, Notification


logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))


def parse_configfile(configfile="/var/lib/lsofmon/config.yml"):
    import yaml
    from yaml import Loader
    with open(configfile, "r") as ymlfile:
        cfg = yaml.load(ymlfile,Loader=Loader)
    return cfg


def get_lsof_value():
    """obtain lsof | wc -l  value"""
    python_pid = os.getpid()
    p1 = subprocess.Popen(["lsof"], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(
        ["grep", "-v", str(python_pid)], stdin=p1.stdout, stdout=subprocess.PIPE
    )
    p1.stdout.close()
    p3 = subprocess.Popen(["wc", "-l"], stdin=p2.stdout, stdout=subprocess.PIPE)
    p2.stdout.close()
    output = p3.communicate()[0]
    count_numbers = output.decode("utf-8").strip()
    return int(count_numbers)


def send_to_statsd(cfg, value):

    config = cfg["statsd"]
    host = config["host"]
    port = config["port"]
    if host not in ["localhost", "127.0.0.1"]:
        try:
            lsof_stats = StatsClient(host, port, prefix="files_opened")
        except:
            logging.error(
                f"Check availablity of {host}:{port} or your network connection!"
            )
            sys.exit(0)
    else:
        lsof_stats = StatsClient(prefix="files_opened")
    logging.info(f"Sending monitoring data (value = {value}) to {host}:{port}...")
    lsof_stats.gauge("lsof", value)
    logging.info(f"Data sent.")


def main():
    
    notify(Notification.READY)
    notify(Notification.STATUS, "Starting lsofmon...")
    config = parse_configfile()
    while True:
        value = get_lsof_value()
        send_to_statsd(config, value)
        time.sleep(60)


if __name__ == "__main__":
    main()
