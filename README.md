# lsofmon


### TODO
1. Functional
   * Detect the number of open files on current OS
   * Report the number to localhost using statsd protocol
2. Non functional
    * Reported values must allow for granularity of at least 1 metric point per minute
    * Reporting must start automatically as part of system start
    * Reporting must be resilient against crashes
    * Implementation must not use more than 100MB of RAM
    * Implementation must be compatible with oneOf{Ubuntu16+, Debian9+, Centos7+}
  

#### Definition of done
* The deliverable must:
* have a documentation on how to rollout and verify
* have tests at your discretion
* be automated at your discretion


### Description
What this app app does is it takes number of open files aquired usin  _lsof | wc -l_ except files that are opened by it's own running python process. Then it connects to StatsD server (by default to localhost:8125) and sends the **gauge** metric with the prefix _files_opened_ and name _lsof_

![Graphite dashboard](graphite.png)

### Installation (tested on CentOS 7)

Download and unzip [zip archive](https://gitlab.com/dimakri/lsofmon/-/archive/master/lsofmon-master.zip) or do

```
git clone https://gitlab.com/dimakri/lsofmon.git
```

go to directory
```
cd lsofmon
```
grant execute on _install.sh_:
```
chmod +x install.sh
```
to change default StatsD server connection setting modify file _config.yml_. Default settings are:

```
statsd:
    host: localhost
    port: 8125
```
start the installation:
```
./install.sh
```


check status of the running lsofmon daemon:

```
[root@localhost lsofmon]# systemctl status lsofmon -l
● lsofmon.service - lsof monitoring service
   Loaded: loaded (/usr/lib/systemd/system/lsofmon.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2019-03-18 15:09:15 MSK; 11s ago
 Main PID: 8364 (python3.6)
   Status: "Starting lsofmon..."
   CGroup: /system.slice/lsofmon.service
           └─8364 /usr/bin/python3.6 /var/lib/lsofmon/lsofmon.py

Mar 18 15:09:16 localhost.localdomain python3.6[8364]: INFO:root:Sending monitoring data (value = 7425) to localhost:8125...
Mar 18 15:09:16 localhost.localdomain python3.6[8364]: INFO:root:Data sent.
Mar 18 15:09:18 localhost.localdomain python3.6[8364]: INFO:root:Sending monitoring data (value = 7425) to localhost:8125...
Mar 18 15:09:18 localhost.localdomain python3.6[8364]: INFO:root:Data sent.
Mar 18 15:09:21 localhost.localdomain python3.6[8364]: INFO:root:Sending monitoring data (value = 7425) to localhost:8125...
Mar 18 15:09:21 localhost.localdomain python3.6[8364]: INFO:root:Data sent.
Mar 18 15:09:23 localhost.localdomain python3.6[8364]: INFO:root:Sending monitoring data (value = 7425) to localhost:8125...

```

### Tests
To run simple tests execute **python3.6 -m pytest test_lsofmon.py**:

```
[root@localhost lsofmon]# python3.6 -m pytest test_lsofmon.py
============================================================================== test session starts ==============================================================================
platform linux -- Python 3.6.7, pytest-4.3.1, py-1.8.0, pluggy-0.9.0
rootdir: /tmp/lsofmon, inifile:
collected 2 items

test_lsofmon.py ..                                                                                                                                                        [100%]

=========================================================================== 2 passed in 1.41 seconds ============================================================================
```


### Possible problems

**Python 3.6 and python36u-pip** might not be installed on your system release.
Make sure python 3.6 is installed.
Make sure that statsd is running on your localhost on port 8125



