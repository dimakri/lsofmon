#!/bin/bash

echo "STOPPING RUNNING LSOFMON SERVICE"
systemctl stop lsofmon
echo "SERVICE STOPPED"

echo "COPING SCRIPT AND CREATINF DIRECTORIES"
mkdir -p /var/lib/lsofmon
cp ./lsofmon.py ./config.yml /var/lib/lsofmon/
cp ./lsofmon.service /lib/systemd/system/lsofmon.service
chmod 644 /lib/systemd/system/lsofmon.service
echo "FINISHED COPYING"
echo "INSTALLING python3.6 , systemd and gcc yum packages"
sudo yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum install -y python36u gcc systemd-devel python36u-pip python36u-devel
echo "INSTALLATION FINISHED"
echo "INSTALLING PIP PACKAGES"
python3.6 -m pip install statsd pyyaml systemd pytest 
echo "FINISHED INSTALLING PIP PACKAGES"

sudo systemctl daemon-reload
sudo systemctl enable lsofmon.service
sudo systemctl start lsofmon.service
systemctl status lsofmon.service 
